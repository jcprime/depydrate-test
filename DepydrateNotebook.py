#!/usr/bin/env python3
# coding: utf-8

import math
import glob
import os
import re

import bondlengths as bls

# Set input and coord files now, just because
# infile = "/home/james/depydrate-test/scol00.inp"
# infile = "/home/james/depydrate-test/scol00-coords.inp"

# MACOS
infile = "/Users/jcprime/depydrate-test/scol00.inp"

# WINDOWS
#infile = "N:\Documents\depydrate-test\scol00.inp"

coordfile = ""


def grablines(file, comment=None):
    """
    Grab all non-comment lines from the specified CP2K input file into a list of strings.

    Parameters
    ==========
        file: string
            path to input file

    Returns
    =======
        lines: list
            each line as a string element
    """
    lines = []
    if comment is None:
        comment = "#"
    with open(file) as infile:
        for line in infile:
            if (line.strip() != "") and (line.strip()[0] == comment):
                continue
            else:
                lines.append(re.sub(r'\t', '    ', line))
    return lines


def searchlines(lines):
    """
    Search list of lines for paths to files and/or coordinate-specific in-file lines, plus cell parameters.

    Parameters
    ==========
        lines: list
            Line-by-line elements of input file

    Returns
    =======
        result: list of dictionaries... or dictionary of lists... ahem
            Filenames, relevant elements of lines list to grab for in-file coordinates section
    """
    proj = ""
    coordlines = []
    celllines = []
    varsetlines = []
    coords = []
    linecount = 0
    for line in lines:
        if ("project".upper() or "project".lower()) in line:
            proj = line
        if ("coord".upper() or "coord".lower()) in line:
            coordlines.append(line)
        if (
               "abc".upper() in line
            or "abc".lower() in line
            or "alpha_beta_gamma".upper() in line
            or "alpha_beta_gamma".lower() in line
            or "angles".upper() in line
            or "angles".lower() in line
            or "multiple_unit_cell".upper() in line
            or "multiple_unit_cell".lower() in line
        ):
            celllines.append(line)
        if ("@set".upper() or "@set".lower()) in line:
            varsetlines.append(line)
        if ("&coord".upper() or "&coord".lower()) in line:
            while ("&end".upper() or "&end".lower()) not in lines[linecount]:
                linecount += 1
                if ("&end".upper() or "&end".lower()) in lines[linecount]:
                    break
                coords.append(lines[linecount])
        linecount += 1
    if coords == []:
        return [proj, coordlines, celllines, varsetlines]
    else:
        return [proj, coordlines, celllines, varsetlines, coords]


def coordfileformat(coordlines, varlines):
    """
    Search for relevant coordinate-related information in the lines from the CP2K input file.

    This is aimed to be more tolerant of the use of variables and referencing within the input files.

    Parameters
    ==========
        coordlines: list
            List of the lines with "coord" in some format in them
        varlines: list
            List of the lines with "@set" in some format in them, in case it needs a separate search by reference
    Returns
    =======
        coordstuff: dict
            Dictionary of {"coordfile": "path/to/file", "coordformat": "format"}
    """
    coordstuff = {}
    coordfilevar = re.compile('.*\s*[^${](?P<keyword>COORD_FILE(?:_NAME)?)\s+\${(?P<var>[A-Za-z0-9.-_]*)}', re.IGNORECASE)
    coordfilepat = re.compile('.*\s*[^${](?P<keyword>COORD_FILE(?:_NAME)?)\s+(?P<path>[^${ ][A-Za-z0-9:\/.-_]*)')
    coordformvar = re.compile('.*\s*[^${](?P<keyword>COORD_FILE_FORMAT|COORDINATE)\s+\${(?P<var>[A-Za-z0-9\/.-_]*)}')
    coordformpat = re.compile('.*\s*[^${](?P<keyword>COORD_FILE_FORMAT|COORDINATE)\s+(?P<value>[A-Za-z0-9\/.-_]*)')
    for line in coordlines:
        filevar = re.search(coordfilevar, line)
        formvar = re.search(coordformvar, line)
        if (filevar != None):
            for varline in varlines:
                if filevar.group('var') in varline:
                    coordstuff["coordfile"] = varline.split()[-1]
        if (formvar != None):
            for varline in varlines:
                if formvar.group('var') in varline:
                    coordstuff["coordformat"] = varline.split()[-1]
    for line in coordlines:
        filepat = re.search(coordfilepat, line)
        formpat = re.search(coordformpat, line)
        if (filepat != None):
            coordstuff["coordfile"] = line.split()[-1]
        if (formpat != None):
            coordstuff["coordformat"] = line.split()[-1]
    return coordstuff


def cellstuff(celllines, varlines):
    """
    Search for relevant coordinate-related information in the lines from the CP2K input file.

    This is aimed to be more tolerant of the use of variables and referencing within the input files.

    Parameters
    ==========
        celllines: list
            List of the lines with "abc" or "alpha_beta_gamma" or "angles" or "multicell" in some format in them
        varlines: list
            List of the lines with "@set" in some format in them, in case it needs a separate search by reference
    Returns
    =======
        cellstuff: dict
            Dictionary of actual values for a, b, c, alpha, beta, gamma, in that order
    """
    cellstuff = {}
    abcvar = re.compile('.*\s*[^${](?P<keyword>ABC)\s+\${(?P<var>[A-Za-z0-9.-_]*)}', re.IGNORECASE)
    abcpat = re.compile('.*\s*[^${](?P<keyword>ABC)\s+(?P<value>[^${ ][A-Za-z0-9:\/.-_]*)', re.IGNORECASE)
    angvar = re.compile('.*\s*[^${](?P<keyword>ALPHA_BETA_GAMMA|ANGLES)\s+\${(?P<var>[A-Za-z0-9.-_]*)}', re.IGNORECASE)
    angpat = re.compile('.*\s*[^${](?P<keyword>ALPHA_BETA_GAMMA|ANGLES)\s+(?P<value>[^${ ][A-Za-z0-9:\/.-_]*)', re.IGNORECASE)
    multicellvar = re.compile('.*\s*[^${](?P<keyword>MULTIPLE_UNIT_CELL)\s+\${(?P<var>[A-Za-z0-9.-_]*)}', re.IGNORECASE)
    multicellpat = re.compile('.*\s*[^${](?P<keyword>MULTIPLE_UNIT_CELL)\s+(?P<value>[^${ ][A-Za-z0-9:\/.-_]*)', re.IGNORECASE)
    for line in celllines:
        lengthvar = re.search(abcvar, line)
        anglevar = re.search(angvar, line)
        mcvar = re.search(multicellvar, line)
        if (lengthvar != None):
            for varline in varlines:
                #and (varline.split()[-1:-4:-1] not in cellstuff)
                if (lengthvar.group('var') in varline):
                    cellstuff['abc'] = varline.split()[-3:]
        if (anglevar != None):
            for varline in varlines:
                if (anglevar.group('var') in varline):
                    cellstuff['alphabetagamma'] = varline.split()[-3:]
        if (mcvar != None):
            for varline in varlines:
                if (mcvar.group('var') in varline):
                    cellstuff['multicell'] = varline.split()[-3:]
    for line in celllines:
        lengthpat = re.search(abcpat, line)
        anglepat = re.search(angpat, line)
        mcpat = re.search(multicellpat, line)
        # and line.split()[-1:-4:-1] not in cellstuff
        if (lengthpat != None):
            cellstuff['abc'] = line.split()[-3:]
        if (anglepat != None):
            cellstuff['alphabetagamma'] = line.split()[-3:]
        if (mcpat != None):
            cellstuff['multicell'] = line.split()[-3:]
    return cellstuff


def grabcoords(coordfile, coordformat="cp2k"):
    """
    Grab coordinate lines from given file.

    TODO: Add support for formats other than CP2K's own (XYZ-like) format.

    Parameters
    ==========
        coordfile: string
            Path to coordinate file
        coordformat: string (optional; defaults to "cp2k")
            Format of coordinates within given coordinate file

    Returns
    =======
        coordlines: list of strings
            Coordinate lines only (ignores e.g. atom count lines, etc.)
    """
    coordlines = []
    with open(coordfile) as file:
        for line in file:
            coord = re.search('(?P<coord>[A-Za-z0-9]*\s+[0-9.]*\s+[0-9.]*\s+[0-9.]*)', line)
            if coord != None:
                coordlines.append(coord.group('coord'))
    return coordlines


def solorfw(coords):
    """
    Splits list of coordinates into separate solvent and framework lists.
    
    Parameters
    ==========
        coords: list
            All coordinates

    Returns
    =======
        fwcoords: list
            All framework coordinates
        solcoords: list
            All solvent coordinates
    """
    fwcoords = []
    solcoords = []
    for line in coords:
        if (
               line.split()[0] == "Si"
            or line.split()[0] == "Al"
            or line.split()[0] == "O2"
            or line.split()[0] == "Na"
            or line.split()[0] == "Ca"
            or line.split()[0] == "K"
            or line.split()[0] == "Mg"
        ):
            fwcoords.append(line)
        elif (
               line.split()[0] == "O1"
            or line.split()[0] == "H"
        ):
            solcoords.append(line)
    return fwcoords, solcoords


def minimage(dxdydz, cell):
    """
    Minimum image convention for a given vector.  Requires cell parameters to enable conversion
    from Cartesian to fractional and back again.  Called by is_bonded().

    Parameters
    ==========
        dxdydz: list
            (Cartesian) x, y, z coordinates for the "image-switch-y" atom
        cell: list
            Cell parameters a, b, c, alpha, beta, gamma, in that order

    Returns
    =======
        micdeltas: list
            dx, dy, dz from minimum image convention
    """
    fracdeltas = []
    frac = cart2frac(cell[0:3], cell[3:6], dxdydz)
    for coord in frac:
        if coord > 0.5:
            fracdeltas.append(coord - 1.0)
        elif coord < -0.5:
            fracdeltas.append(coord + 1.0)
        elif (coord > -0.5) and (coord < 0.5):
            fracdeltas.append(coord)
        else:
            print("Something's gone squiffy; your fractionals aren't very fraction-y...")
            break
    micdeltas = frac2cart(cell[0:3], cell[3:6], fracdeltas)
    return micdeltas


def is_bonded(atom1, atom2, cell, pbc=True):
    """
    Tests whether two atoms are within bonding distance of each other.
    
    Parameters
    ==========
        atom1: list
            Atom 1's symbol (on the Periodic Table, not the "O1" or "O2" that we use to distinguish fw/solvent O),
            x coordinate, y coordinate, and z coordinate
        atom2: list
            Atom 2's symbol (on the Periodic Table, not the "O1" or "O2" that we use to distinguish fw/solvent O),
            x coordinate, y coordinate, and z coordinate
        cell: list
            Cell parameters a, b, c, alpha, beta, gamma, in that order
        pbc: bool
            True if periodic boundary conditions are to be used (default), False otherwise

    Returns
    =======
        True or False: bool
            True if Atom 1 and Atom 2 are within bonding distance of each other, otherwise False
    """
    # Note that this doesn't account for atoms being provided in the opposite order (yet)!
    reallength = bls.bondlengths[atom1[0][0:1] + '-' + atom2[0][0:1]]
    bondtol = 0.5

    dx = atom1[1] - atom2[1]
    dy = atom1[2] - atom2[2]
    dz = atom1[3] - atom2[3]

    dxmic, dymic, dzmic = minimage([dx, dy, dz], cell)

    if (pbc == True):
        distance = math.sqrt(dxmic**2 + dymic**2 + dzmic**2)
    else:
        distance = math.sqrt(dx**2 + dy**2 + dz**2)

    if distance <= (reallength + bondtol):
        return True
    else:
        return False


def findmol(atoms, cell):
    """
    Finds molecules, assumed to be water, from a list of atoms with their coordinates,
    and assigns them to a list (of lists) accordingly.

    TODO: Add periodic boundary conditions minimum image convention stuff

    Parameters
    ==========
        atoms: list
            Atoms and coordinates as strings
        cell: list
            Cell parameters a, b, c, alpha, beta, gamma, in that order

    Returns
    =======
        molecules: list of lists
            Atoms and coordinates collected into distinct lists according to their molecule-y-ness!
    """
    molecules = []
    leftovers = []
    line1count = -1
    for line in atoms:
        line1count += 1
        atom1 = line.split()
        atom1[1:] = [float(n) for n in atom1[1:]]
        # TODO: IF NOT CARTESIAN, MUST CONVERT TO CARTESIAN BEFORE CONTINUING!
        if atom1[0] == "O1":
            mol = []
            mol.append(atoms.pop(line1count))
            # Process through, looking for no more than 2 H atoms which are within bonding distance of it
            line2count = -1
            for line2 in atoms:
                line2count += 1
                atom2 = line2.split()
                atom2[1:] = [float(n) for n in atom2[1:]]
                # If it's H, and there's not yet 2 H lines in the molecule,
                # AND they're within bonding distance of each other, grab that line out of the list
                # and append it to mol
                if (atom2[0] == "H") and (len(mol) < 3) and (is_bonded(atom1, atom2, cell, True) == True):
                    mol.append(atoms.pop(line2count))
            molecules.append(mol)
    return molecules, leftovers


def frac2cart(lengths, angles, uvw):
    """
    Parameters
    ==========
        lengths: list
            a, b, c cell lengths
        angles: list
            alpha, beta, gamma cell angles
        uvw: list
            u, v, w fractional coordinates

    Returns
    =======
        xyz: list
            x, y, z Cartesian coordinates
    """
    # CP2K defaults to providing angles in degrees, so convert below
    radangles = []
    for deg in angles:
        rad = math.radians(deg)
        radangles.append(rad)
    angles = radangles

    # Unpack all the things for ease of equation-ing
    a, b, c = lengths
    alpha, beta, gamma = angles
    u, v, w = uvw

    volume = a * b * c * math.sqrt(1 - (math.cos(alpha))**2 - (math.cos(beta))**2 - (math.cos(gamma))**2             + (2 * math.cos(alpha) * math.cos(beta) * math.cos(gamma)))

    # Use matrix from Wikipedia to convert to Cartesian coordinates
    x = (a * u) + ((b * math.cos(gamma)) * v) + ((c * math.cos(beta)) * w)
    y = (0 * u) + ((b * math.sin(gamma)) * v)         + ((c * ((math.cos(alpha) - (math.cos(beta) * math.cos(gamma))) / math.sin(gamma))) * w)
    z = (0 * u) + (0 * v) + ((volume / (a *  b * math.sin(gamma))) * w)
    xyz = [x, y, z]
    return xyz


def cart2frac(lengths, angles, xyz):
    """
    Parameters
    ==========
        lengths: list
            a, b, c cell lengths
        angles: list
            alpha, beta, gamma cell angles
        xyz: list
            x, y, z Cartesian coordinates

    Returns
    =======
        uvw: list
            u, v, w fractional coordinates
    """
    # CP2K defaults to providing angles in degrees, so convert below
    radangles = []
    for deg in angles:
        rad = math.radians(deg)
        radangles.append(rad)
    angles = radangles
    
    # Unpack all the things
    a, b, c = lengths
    alpha, beta, gamma = angles
    x, y, z = xyz

    volume = a * b * c * math.sqrt(1 - (math.cos(alpha))**2 - (math.cos(beta))**2 - (math.cos(gamma))**2             + (2 * math.cos(alpha) * math.cos(beta) * math.cos(gamma)))

    # Use matrix from Wikipedia to convert to fractional coordinates
    u = ((1 / a) * x) + (-1 * (math.cos(gamma)/(a * math.sin(gamma))) * y)         + ((b * c * (((math.cos(alpha) * math.cos(gamma)) - math.cos(beta)) / (volume * math.sin(gamma))) * z))
    v = (0 * x) + ((1 / (b * math.sin(gamma))) * y)         + ((a * c * (((math.cos(beta) * math.cos(gamma)) - math.cos(alpha)) / (volume * math.sin(gamma))) * z))
    w = (0 * x) + (0 * y) + (((a * b * math.sin(gamma)) / volume) * z)
    uvw = [u, v, w]
    return uvw


def writeoutput(project, alllines, framework, molecules):
    """
    Writes output files, substituting the original project name and filenames with new,
    removing the MULTIPLE_UNIT_CELL stuff (because of a quirk in CP2K's output),
    removing the &COORD section and its content if it exists (as it would in a restart file, for example)
    """
    pass



# Grab list of lines in the input file
lines = grablines(infile)

# Grab all the important lines from the input file - namely the project name, coordinate-related lines,
# cell-parameter-related lines, and variable-setting lines, potentially also with inline-coordinate lines if found
if len(searchlines(lines)) > 4:
    project, coordlines, celllines, varlines, coords = searchlines(lines)
else:
    project, coordlines, celllines, varlines = searchlines(lines)

# Grab the coordinate file and format information from the input file
coordstuff = coordfileformat(coordlines, varlines)

# Grab the cell parameters and multicell information from the input file
cellparams = cellstuff(celllines, varlines)

# Get the numeric stuff out of string format!
newcps = {}
for key, lst in cellparams.items():
    newlst = []
    for i in lst:
        if "." in i:
            newlst.append(float(i))
        else:
            newlst.append(int(i))
    newcps[key] = newlst
cellparams = newcps

# Wanting a single flattened list to pass to the coordinate-converting functions:
cell = cellparams['abc'] + cellparams['alphabetagamma']

# Now to get the actual coordinates, either from a given path to a separate file or inline (within the `&COORD` section).
if len(searchlines(lines)) == 5:
#     print("We have inline coordinates!")
    coordlines = coords
else:
#     print("We need to grab the coordinates from a separate file!")
    coordlines = grabcoords(coordstuff['coordfile'])

# The Big Test, namely whether or not the solvent molecules are found as you'd expect, or at least as can be compared to the original GULP-specific `dehydrate` output.
fw, solv = solorfw(coordlines)
mols, remainder = findmol(solv, cell)
