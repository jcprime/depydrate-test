#!/usr/bin/env python3
import math
import glob
import os
import re

# My modules
import elements
import bondlengths as bls
import definitions as dfns
import mymaths

# Set input and coord files now just because
infile = "/home/james/depydrate-test/scol00.inp"
# infile = "/home/james/depydrate-test/scol00-coords.inp"

# infile = "/home/soft/depydrate-test/scol00.inp"
# infile = "/home/soft/depydrate-test/scol00-coords.inp"
coordfile = ""

def grablines(file, comment=None):
    """
    Grab all non-comment lines from the specified CP2K input file into a list of strings.

    Parameters
    ==========
        file: string
            path to input file

    Returns
    =======
        lines: list
            each line as a string element
    """
    lines = []
    if comment is None:
        comment = "#"
    with open(file) as infile:
        for line in infile:
            if (line.strip() != "") and (line.strip()[0] == comment):
                continue
            else:
                lines.append(re.sub(r'\t', '    ', line))
    return lines

def searchlines(lines):
    """
    Search list of lines for paths to files and/or coordinate-specific in-file lines.

    Parameters
    ==========
        lines: list
            Line-by-line elements of input file

    Returns
    =======
        result: list of dictionaries... or dictionary of lists :D
            Filenames, relevant elements of lines list to grab for in-file coordinates section
    """
    proj = ""
    filter1 = []
    filter2 = []
    coords = []
    linecount = 0
    for line in lines:
        if ("coord".upper() or "coord".lower()) in line:
            filter1.append(line)
        if ("@set".upper() or "@set".lower()) in line:
            filter2.append(line)
        if ("project".upper() or "project".lower()) in line:
            proj = line
        if ("&coord".upper() or "&coord".lower()) in line:
            while ("&end".upper() or "&end".lower()) not in lines[linecount]:
                linecount += 1
                if ("&end".upper() or "&end".lower()) in lines[linecount]:
                    break
                coords.append(lines[linecount])
                #print(lines[linecount])
        linecount += 1
    if coords == []:
        return [proj, filter1, filter2]
    else:
        return [proj, filter1, filter2, coords]

def coordfileformat(coordlines, varlines):
    """
    Search for relevant coordinate-related information in the lines from the CP2K input file.

    This is aimed to be more tolerant of the use of variables and referencing within the input files.

    Parameters
    ==========
        coordlines: list
            List of the lines with "coord" in some format in them
        varlines: list
            List of the lines with "@set" in some format in them, in case it needs a separate search by reference
    Returns
    =======
        coordstuff: dict
            Dictionary of {"coordfile": "path/to/file", "coordformat": "format"}
    """
    coordstuff = {}
    coordfilevar = re.compile('.*\s*[^${](?P<keyword>COORD_FILE(?:_NAME)?)\s+\${(?P<var>[A-Za-z0-9.-_]*)}', re.IGNORECASE)
    coordfilepat = re.compile('.*\s*[^${](?P<keyword>COORD_FILE(?:_NAME)?)\s+(?P<path>[^${ ][A-Za-z0-9:\/.-_]*)')
    coordformvar = re.compile('.*\s*[^${](?P<keyword>COORD_FILE_FORMAT|COORDINATE)\s+\${(?P<var>[A-Za-z0-9\/.-_]*)}')
    coordformpat = re.compile('.*\s*[^${](?P<keyword>COORD_FILE_FORMAT|COORDINATE)\s+(?P<value>[A-Za-z0-9\/.-_]*)')
    for line in coordlines:
        filevar = re.search(coordfilevar, line)
        formvar = re.search(coordformvar, line)
#         print("filevar:", filevar)
#         print("formvar:", formvar)
        if (filevar != None):
#             print(filevar.group('keyword'), filevar.group('var'))
            for varline in varlines:
                if filevar.group('var') in varline:
#                     print(varline.split()[-1])
                    coordstuff["coordfile"] = varline.split()[-1]
        if (formvar != None):
#             print(formvar.group('keyword'), formvar.group('var'))
            for varline in varlines:
                if formvar.group('var') in varline:
#                     print(varline.split()[-1])
                    coordstuff["coordformat"] = varline.split()[-1]
    for line in coordlines:
        filepat = re.search(coordfilepat, line)
        formpat = re.search(coordformpat, line)
#         print("filepat:", filepat)
#         print("formpat:", formpat)
        if (filepat != None):
#             print(filepat.group('keyword'), filepat.group('path'))
            coordstuff["coordfile"] = line.split()[-1]
        if (formpat != None):
#             print(formpat.group('keyword'), formpat.group('value'))
            coordstuff["coordformat"] = line.split()[-1]
    return coordstuff

def grabcoords(coordfile, coordformat="cp2k"):
    """
    Grab coordinate lines from given file.

    TODO: Add support for formats other than CP2K's own (XYZ-like) format.

    Parameters
    ==========
        coordfile: string
            Path to coordinate file
        coordformat: string (optional; defaults to "cp2k")
            Format of coordinates within given coordinate file

    Returns
    =======
        coordlines: list of strings
            Coordinate lines only (ignores e.g. atom count lines, etc.)
    """
    coordlines = []
    with open(coordfile) as file:
        for line in file:
            coord = re.search('(?P<coord>[A-Za-z0-9]*\s+[0-9.]*\s+[0-9.]*\s+[0-9.]*)', line)
            if coord != None:
                coordlines.append(coord.group('coord'))
    return coordlines

def solorfw(coords):
    """
    Splits list of coordinates into separate solvent and framework lists.
    
    Parameters
    ==========
        coords: list
            All coordinates

    Returns
    =======
        fwcoords: list
            All framework coordinates
        solcoords: list
            All solvent coordinates
    """
	pass

lines = grablines(infile)
lines
filelines = searchlines(lines)
filelines
coordstuff = coordfileformat(filelines[1], filelines[2])

if len(filelines) == 4:
#     print("We have inline coordinates!")
    coordlines = filelines[3]
else:
#     print("We need to grab the coordinates from a separate file!")
    coordlines = grabcoords(coordstuff['coordfile'])
# print(coordlines)
