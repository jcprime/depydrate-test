# TODO

Currently based on original `dehydrate`.

## Constants

* Default atom notations to be dehydrated

## Variables

* Count of total atoms (initially zero)
* Count of total molecules (initially zero)
* Count of total framework atoms (initially zero)
* Count of total solvent atoms (initially zero)
* Total line counter (initially 1)
* Line at which coordinates start (*if they start in the same file!*)

## Loops

* `while` iterating over all lines in input file
    - Effectively `while True` (Python) type loop
    - Grab current line as single string
    - If current line is not a comment, dump, or output line
        + Add line to array of lines
        + Increment line counter
        + If *thing specifying cell parameters* is found in **line STRING** (i.e. not incremented line!  This is for a reason!), set PBC flag on and grab **next (incremented as above) line** as a string and extract said parameters from assumed format, then increment line counter again
        + Else if *thing specifying coordinates starting* OR *anything related to coordinates* is found in line, exit loop

## Functions

* `generate_neighbours`

## Questions

* What is a "file descriptor" when it's at home?  (It keeps getting mentioned in `pydoc` is all)
* Numerical Recipes in C index sort - what and why?
* Is there a C(++) equivalent of `pydoc` for offline use?
* Equivalent to Python's `list.pop()` method in C(++)?
* Can I make a class for elements, where their constants are defined depending on the string passed to the class constructor?

01793888585 (missed call)? <-- Norwood Consulting, re: Nationwide survey