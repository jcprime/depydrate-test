#!/usr/bin/env python3
# mymaths.py
# Vector dot and cross products, neighbour-generating stuff

import math
import bondlengths as bls

# Normalisation factor for error function is as given below
errorfunction = (2.0/math.sqrt(math.pi))

def find_bondlength(atom1, atom2):
    """Looks up bond length in bondlengths.py

    Parameters
    ==========
        atom1: String of element symbol
        atom2: String of element symbol

    Returns
    =======
        Bond length as float, in Angstroms
    """
    bond = str(atom1) + "-" + str(atom2)
    altbond = str(atom2) + "-" + str(atom1)
    if bls.bondlengths[bond] == True:
        bondlength = bls.bondlengths[bond]
    elif bls.bondlengths[altbond] == True:
        bondlength = bls.bondlengths[altbond]
    else:
        raise SomeKindOfError
    return bondlength


def vec_dot(vec1, vec2):
    """Calculate vector dot product

    Parameters
    ==========
        vec1: List or tuple of vector elements
        vec2: List or tuple of vector elements

    Returns
    =======
        Dot product as float
    """
    dot_product = 0
    if len(vec1) == len(vec2):
        for i in range(0,len(vec1)):
            dot_product += (vec1[i] * vec2[i])
    else:
        print("Vectors are not the same length!")
    return dot_product


def vec_cross(vec1, vec2):
    """Calculate vector cross product

    Parameters
    ==========
        vec1: List (or tuple) of vector elements
        vec2: List (or tuple) of vector elements

    Returns
    =======
        cross_product: List of cross-product vector elements
    """
    cross_product = []
    def determinant(detvec1, detvec2):
        """MUST be a 2x2 matrix
        """
        return (detvec1[0]*detvec2[1] - detvec1[1]*detvec2[0])

    if len(vec1) == len(vec2):
        i = 1
        j = 1
        k = 1
        matrix = [[i, j, k], vec1, vec2]
        # cross_product = [matrix[0][0]*determinant(matrix[1][1:], matrix[2][1:]), matrix[0][1]*determinant(matrix[0][1:], matrix[2][1:]), matrix[0][2]*determinant(matrix[0][1:], matrix[1][1:])]
        for num in range(0, len(vec1)):
            cross_product.append(matrix[0][num]*determinant(matrix[num-1][1:], matrix[num-1][1:]))
    else:
        print("Vectors are not the same length!")
    return cross_product


def generate_neighbours():
    """
    """
    pass
